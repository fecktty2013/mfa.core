using MFA.Core.Service.Entities;
using MFA.Core.Service.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Repository = MFA.Core.Service.Repositories;

namespace MFA.Core.Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MenuController : ControllerBase
    {
        private readonly IConfiguration configuration;
        // private readonly Repositories.AppContext appContext;
        private readonly IRepository<Menu> menuRepository;

        public MenuController(IConfiguration configuration,
            IRepository<Menu> menuRepository)
        {
            this.configuration = configuration;
            this.menuRepository = menuRepository;
        }

        [HttpGet("list")]
        public System.Collections.Generic.IEnumerable<Menu> List()
        {
            return menuRepository.List();
        }
    }
}