USE [MFA]
GO

/****** Object:  Table [dbo].[MENUS]    Script Date: 2020-03-31 11:19:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MENUS]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MENUS](
	[MenuId] [uniqueidentifier] NOT NULL,
	[MenuName] [nvarchar](50) NOT NULL,
	[LinkPath] [varchar](500) NOT NULL,
	[ParentMenuId] [uniqueidentifier] NULL,
	[LastModifiedAt] [datetime2](7) NULL,
	[LastModifiedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_MENUS] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_MENUS_MenuId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MENUS] ADD  CONSTRAINT [DF_MENUS_MenuId]  DEFAULT (newid()) FOR [MenuId]
END
GO


