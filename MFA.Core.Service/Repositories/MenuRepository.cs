using System;
using System.Collections.Generic;
using System.Linq;
using MFA.Core.Service.Entities;
using Microsoft.EntityFrameworkCore;
using Repositories = MFA.Core.Service.Repositories;

namespace MFA.Core.Service.Repositories {

    public class MenuRepository : IRepository<Menu>
    {
        private Repositories.AppContext appContext;
        public MenuRepository(Repositories.AppContext appContext)
        {
            this.appContext = appContext;
        }
        public IList<Menu> List()
        {
            return this.appContext.Menus.ToList();
        }
    }
}