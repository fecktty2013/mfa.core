using MFA.Core.Service.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace MFA.Core.Service.Repositories
{
    public class AppContext : DbContext
    {
        public AppContext(DbContextOptions<AppContext> options) : base(options)
        {
        }

        public virtual DbSet<Menu> Menus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // optionsBuilder.UseSqlServer("Server=(LocalDB)\\MSSQLLocalDB;AttachDbFileName=\"C:\\USERS\\WEI ZHOU\\MFA.MDF\";Integrated Security=True");
                optionsBuilder.UseInMemoryDatabase("MFA-CORE");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Menu>()
                .HasData(
                    new Menu() { MenuId = new Guid("A18BBFFA-6122-41C8-874A-13864230559C"), MenuName = "Home", LinkPath = "/", ParentMenuId = null, LastModifiedAt = DateTime.Parse("2020-03-31 11:24:54.4900000"), LastModifiedBy = "Wei Zhou" }
                );
        }
    }
}