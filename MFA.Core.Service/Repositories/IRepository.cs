using System.Collections.Generic;

namespace MFA.Core.Service.Repositories
{
    public interface IRepository<T>
    {
        IList<T> List();
    }
}