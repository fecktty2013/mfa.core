const path = require('path');
const AntdDayjsWebpackPlugin = require('antd-dayjs-webpack-plugin');

const plugins = [
    new AntdDayjsWebpackPlugin()
];

if (process.env.ANALYZE) {
    const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
    plugins.push(new BundleAnalyzerPlugin());
}

module.exports = {
    entry: {
        index: path.resolve(__dirname, 'index.js')
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        chunkFilename: '[name]-[chunkhash:5].js',
        jsonpFunction: 'jsonpAppWrapper'
    },
    resolve: {
        extensions: ['.js', '.json', '.jsx'],
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /antd.*\.css$/,
                use: ["style-loader", "css-loader?modules=false"],
            },
            {
                test: /.(css|scss)$/,
                exclude: /antd.*\.css$/,
                use: [
                    "style-loader",
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                        }
                    },
                    "sass-loader"
                ],
            },
        ]
    },
    plugins
}