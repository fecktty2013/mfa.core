import * as React from 'react';
import { render } from 'react-dom';
import $before from '../utils/before';
import $after from '../utils/after';
import App from './App';

try {
    $before('.App', `<div id="AppWrapper" data-version="0.1.3" data-env="dev">`);
    $after('.App', `<div id="LeftPad">`);
} catch (e) {
    if (e instanceof TypeError && /insertAdjacentHTML/.test(e.message)) {
        console.warn('The root of your app should be under <div class="App"></div>');
    } else {
        console.error(e);
    }
}

render(
    <App />,
    document.getElementById('AppWrapper')
);