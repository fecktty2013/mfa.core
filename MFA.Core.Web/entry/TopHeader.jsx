import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Button, Avatar, Icon } from 'antd';
import style from './style.scss';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

function TopHeader(props) {
    const { folded = false, userName = 'Anonymous', authenticated = true } = props;

    let logout = () => {

    };

    return (
        <Header className={style.header}>
            {
                folded
                ?
                <div onClick={props.handleSidebarDisplay} className={style.sideToggler}>
                    <Icon type="menu-unfold" /></div>
                :
                <div onClick={props.handleSidebarDisplay} className={style.sideToggler}>
                    <Icon type="menu-fold" /></div>
            }
            <span className={style.avatar}>
                <Avatar className={style.avatarImg} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                <span>{userName}</span>
                {authenticated ? <Button type="link" onClick={logout}>Log Out</Button> : undefined}
            </span>
        </Header>
    );
}

export default TopHeader;
