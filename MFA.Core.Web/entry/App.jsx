import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Button, Avatar } from 'antd';
import Sidebar from './Sidebar';
import TopHeader from './TopHeader';
import element from "../utils/element";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

import style from './style.scss';

function App(props) {

    const [folded, setFolded] = useState(false);

    const handleSidebarDisplay = () => {
        if (folded) {
            element.removeClass(document.body, 'side-collapsed');
        } else {
            element.addClass(document.body, 'side-collapsed');
        }
        setFolded(!folded)
    };

    return (
        <div id="appRoot">
            <Layout>
                <Sidebar folded={folded} pathname={window.location.pathname} />
                <TopHeader folded={folded} handleSidebarDisplay={handleSidebarDisplay} />
            </Layout>
        </div>
    );
}

export default App;
