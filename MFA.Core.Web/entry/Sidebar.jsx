import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Button, Avatar, Icon } from 'antd';
import classNames from 'classnames';
import string from '../utils/string';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

import style from './style.scss';

function Sidebar(props) {
    let selectedKeys = [];
    let defaultOpenKeys = [];
    // menu { href, pageId, subMenus, icon, text }
    // subMenu { pageId, href, text }
    let menus = props.menus || [
        {
            pageId: 'summary-summary',
            icon: 'dashboard',
            text: 'Dashboard',
            href: '/bi',
        },
        {
            pageId: 'product',
            icon: 'table',
            text: 'Product',
            subMenus: [
                { pageId: 'media-video', text: 'Video', href: '/product/video' },
                { pageId: 'media-audio', text: 'Audio', href: '/product/audio' },
            ]
        },
        {
            pageId: 'user',
            icon: 'user',
            text: 'User',
            subMenus: [
                { pageId: 'user-user_summary', text: 'Overview', href: '/user/manage' },
                { pageId: 'user-head_img', text: 'Avatars', href: '/user/avatars' },
                { pageId: 'user-user_invite', text: 'Distributor', href: '/user/distribution' },
            ]
        },
        {
            pageId: 'category-category',
            icon: 'tags',
            text: 'Category',
            href: '/category',
        },
        {
            pageId: 'operation',
            icon: 'setting',
            text: 'Operation',
            subMenus: [
                { pageId: 'operation-base', text: 'Basic', href: '/operation/manage' },
                { pageId: 'operation-config', text: 'Settings', href: '/operation/settings' },
            ]
        },
        {
            pageId: 'fund-fund',
            icon: 'pay-circle',
            text: 'Financial',
            href: '/finance',
        },
        {
            pageId: 'config-config',
            icon: 'sliders',
            text: 'Configuration',
            href: '/settings',
        },
        {
            pageId: 'comment',
            icon: 'message',
            text: 'Comment',
            subMenus: [
                { pageId: 'comment-management', text: 'Comments', href: '/comments' },
                { pageId: 'comment-robot', text: 'Robots', href: '/comments/robots' },
            ]
        },
        {
            pageId: 'account-account',
            icon: 'team',
            text: 'Accounts',
            href: '/accounts',
        },
        {
            pageId: 'log-log',
            icon: 'read',
            text: 'Logging',
            href: '/logging',
        },
        {
            pageId: 'system-system',
            icon: 'apartment',
            text: 'System',
            href: '/system',
        },
    ];

    for (let menuIndex = 0; menuIndex < menus.length; menuIndex++) {
        const menu = menus[menuIndex];
        if (menu.href === props.pathname) {
            selectedKeys.push(menu.pageId);
            break;
        }
        const subMenus = menu.subMenus || [];
        for (let subMenuIndex = 0; subMenuIndex < subMenus.length; subMenuIndex++) {
            const subMenu = subMenus[subMenuIndex];
            if (subMenu.href === props.pathname) {
                selectedKeys.push(subMenu.pageId);
                defaultOpenKeys.push(menu.pageId);
                break;
            }
        }
    }

    const { folded =false } = props;

    return (
        <Sider collapsible collapsed={folded} 
            className={classNames({
                [style.sider]: true,
                collapsed: folded
            })} trigger={null}>
            <div className={classNames(style.logo, 'logo')} />
            <Menu theme="dark"
                mode="inline" 
                inlineCollapsed={folded}
                forceSubMenuRender
                selectedKeys={selectedKeys}
                defaultOpenKeys={defaultOpenKeys}>
                {menus.map(menu => {
                    return menu.subMenus ? <SubMenu key={menu.pageId} title={
                        <span>
                            <Icon type={menu.icon} />
                            <span>{menu.text}</span>
                        </span>
                    }>
                        {menu.subMenus.map(subMenu => (
                            <Menu.Item key={subMenu.pageId}>
                                <a href={subMenu.href}>{subMenu.text}</a>
                            </Menu.Item>
                        ))}
                    </SubMenu> : <Menu.Item key={menu.pageId} title={menu.text}>
                            <a href={menu.href}>
                                <Icon type={menu.icon} />
                                <span>{menu.text}</span>
                            </a>
                        </Menu.Item>
                })}
            </Menu>
        </Sider>
    );
}

export default Sidebar;
