module.exports = function $before(selector, html) {
    var el = document.querySelector(selector);
    el.insertAdjacentHTML('beforebegin', html);
    return el;
}