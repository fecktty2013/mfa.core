module.exports = {
    hungarianToPascal(str) {
        let newStr = [];
        let isFirstLetter = true;
        for (let i=0; i<str.length; i++) {
            if (str[i] == '-') {
                isFirstLetter = true;
                break;
            }
            newStr.push(isFirstLetter ? str[i].toUpperCase() : str[i]);
            isFirstLetter = false;
        }
        return newStr.join('');
    }
};
