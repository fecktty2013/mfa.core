module.exports = {
    addClass(ele, className) {
        let classNames = ele.className.split(" ");
        if (classNames.indexOf(className) < 0) {
            classNames.push(className);
        }
        ele.className = classNames.join(' ');
    },
    removeClass(ele, className) {
        let classNames = ele.className.split(" ");
        let findIndex = classNames.indexOf(className);
        while (findIndex >= 0) {
            classNames.splice(findIndex, 1);
            findIndex = classNames.indexOf(className);
        }
        ele.className = classNames.join(' ');
    }
}