module.exports = function $after(selector, html) {
    var el = document.querySelector(selector);
    el.insertAdjacentHTML('afterend', html);
    return el;
}